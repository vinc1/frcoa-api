package com.frcoa.frcoaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrcoaApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrcoaApiApplication.class, args);
    }

}
