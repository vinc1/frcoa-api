package com.frcoa.frcoaapi.data.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "industryActivityMaterials")
@IdClass(IndustryActivityMaterialsPK.class)
public class IndustryActivityMaterials {
    @Basic
    @Id
    @Column(name = "typeID")
    private Integer typeId;
    @Basic
    @Id
    @Column(name = "activityID")
    private Integer activityId;
    @Basic
    @Id
    @Column(name = "materialTypeID")
    private Integer materialTypeId;
    @Basic
    @Column(name = "quantity")
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "typeID", referencedColumnName = "typeId", insertable = false, updatable = false)
    @JoinColumn(name = "activityID", referencedColumnName = "activityID", insertable = false, updatable = false)
    private IndustryActivityProducts productRef;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "materialTypeID", referencedColumnName = "typeId", insertable = false, updatable = false)
    private InvTypes materialProductType;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public Integer getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(Integer materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public InvTypes getMaterialProductType() {
        return materialProductType;
    }

    public void setMaterialProductType(InvTypes materialProductType) {
        this.materialProductType = materialProductType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndustryActivityMaterials that = (IndustryActivityMaterials) o;
        return Objects.equals(typeId, that.typeId) && Objects.equals(activityId, that.activityId) && Objects.equals(materialTypeId, that.materialTypeId) && Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeId, activityId, materialTypeId, quantity);
    }
}
