package com.frcoa.frcoaapi.data.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="invGroups")
public class InvGroups {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "groupID")
    private Integer groupId;
    @Basic
    @Column(name = "categoryID")
    private Integer categoryId;
    @Basic
    @Column(name = "groupName")
    private String groupName;
    @Basic
    @Column(name = "iconID")
    private Integer iconId;
    @Basic
    @Column(name = "useBasePrice")
    private Byte useBasePrice;
    @Basic
    @Column(name = "anchored")
    private Byte anchored;
    @Basic
    @Column(name = "anchorable")
    private Byte anchorable;
    @Basic
    @Column(name = "fittableNonSingleton")
    private Byte fittableNonSingleton;
    @Basic
    @Column(name = "published")
    private Byte published;

    @ManyToOne
    @JoinColumn(name = "categoryID", referencedColumnName = "categoryID", insertable = false, updatable = false)
    private InvCategories category;

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getIconId() {
        return iconId;
    }

    public void setIconId(Integer iconId) {
        this.iconId = iconId;
    }

    public Byte getUseBasePrice() {
        return useBasePrice;
    }

    public void setUseBasePrice(Byte useBasePrice) {
        this.useBasePrice = useBasePrice;
    }

    public Byte getAnchored() {
        return anchored;
    }

    public void setAnchored(Byte anchored) {
        this.anchored = anchored;
    }

    public Byte getAnchorable() {
        return anchorable;
    }

    public void setAnchorable(Byte anchorable) {
        this.anchorable = anchorable;
    }

    public Byte getFittableNonSingleton() {
        return fittableNonSingleton;
    }

    public void setFittableNonSingleton(Byte fittableNonSingleton) {
        this.fittableNonSingleton = fittableNonSingleton;
    }

    public Byte getPublished() {
        return published;
    }

    public void setPublished(Byte published) {
        this.published = published;
    }

    public InvCategories getCategory() {
        return category;
    }

    public void setCategory(InvCategories category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvGroups invGroups = (InvGroups) o;
        return groupId == invGroups.groupId && Objects.equals(categoryId, invGroups.categoryId) && Objects.equals(groupName, invGroups.groupName) && Objects.equals(iconId, invGroups.iconId) && Objects.equals(useBasePrice, invGroups.useBasePrice) && Objects.equals(anchored, invGroups.anchored) && Objects.equals(anchorable, invGroups.anchorable) && Objects.equals(fittableNonSingleton, invGroups.fittableNonSingleton) && Objects.equals(published, invGroups.published);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId, categoryId, groupName, iconId, useBasePrice, anchored, anchorable, fittableNonSingleton, published);
    }
}
