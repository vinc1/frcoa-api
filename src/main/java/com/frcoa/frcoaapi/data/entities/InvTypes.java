package com.frcoa.frcoaapi.data.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Entity(name = "invTypes")
@Table(name = "invTypes")
public class InvTypes {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "typeID")
    private Integer typeId;
    @Basic
    @Column(name = "groupID")
    private Integer groupId;
    @Basic
    @Column(name = "typeName")
    private String typeName;
    @Basic
    @Column(name = "description")
    private String description;
    @Basic
    @Column(name = "mass")
    private Double mass;
    @Basic
    @Column(name = "volume")
    private Double volume;
    @Basic
    @Column(name = "capacity")
    private Double capacity;
    @Basic
    @Column(name = "portionSize")
    private Integer portionSize;
    @Basic
    @Column(name = "raceID")
    private Integer raceId;
    @Basic
    @Column(name = "basePrice")
    private BigDecimal basePrice;
    @Basic
    @Column(name = "published")
    private Byte published;
    @Basic
    @Column(name = "marketGroupID")
    private Integer marketGroupId;
    @Basic
    @Column(name = "iconID")
    private Integer iconId;
    @Basic
    @Column(name = "soundID")
    private Integer soundId;
    @Basic
    @Column(name = "graphicID")
    private Integer graphicId;

    @ManyToOne
    @JoinColumn(name = "groupId", referencedColumnName = "groupId")
    private InvGroups group;

    @OneToMany(mappedBy = "type", cascade = CascadeType.ALL)
    private Collection<InvTypeMaterials> materiaux;

    @OneToMany(mappedBy = "productType", cascade = CascadeType.ALL)
    private Collection<IndustryActivityProducts> blueprints;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMass() {
        return mass;
    }

    public void setMass(Double mass) {
        this.mass = mass;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public Integer getPortionSize() {
        return portionSize;
    }

    public void setPortionSize(Integer portionSize) {
        this.portionSize = portionSize;
    }

    public Integer getRaceId() {
        return raceId;
    }

    public void setRaceId(Integer raceId) {
        this.raceId = raceId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public Byte getPublished() {
        return published;
    }

    public void setPublished(Byte published) {
        this.published = published;
    }

    public Integer getMarketGroupId() {
        return marketGroupId;
    }

    public void setMarketGroupId(Integer marketGroupId) {
        this.marketGroupId = marketGroupId;
    }

    public Integer getIconId() {
        return iconId;
    }

    public void setIconId(Integer iconId) {
        this.iconId = iconId;
    }

    public Integer getSoundId() {
        return soundId;
    }

    public void setSoundId(Integer soundId) {
        this.soundId = soundId;
    }

    public Integer getGraphicId() {
        return graphicId;
    }

    public void setGraphicId(Integer graphicId) {
        this.graphicId = graphicId;
    }

    public InvGroups getGroup(){
        return group;
    }

    public void setGroup(InvGroups group){
        this.group = group;
    }

    public Collection<InvTypeMaterials> getMateriaux() {
        return materiaux;
    }

    public void setMateriaux(Collection<InvTypeMaterials> materiaux) {
        this.materiaux = materiaux;
    }

    public Collection<IndustryActivityProducts> getBlueprints() {
        return blueprints;
    }

    public void setBlueprints(Collection<IndustryActivityProducts> blueprints) {
        this.blueprints = blueprints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvTypes invTypes = (InvTypes) o;
        return typeId == invTypes.typeId && Objects.equals(groupId, invTypes.groupId) && Objects.equals(typeName, invTypes.typeName) && Objects.equals(description, invTypes.description) && Objects.equals(mass, invTypes.mass) && Objects.equals(volume, invTypes.volume) && Objects.equals(capacity, invTypes.capacity) && Objects.equals(portionSize, invTypes.portionSize) && Objects.equals(raceId, invTypes.raceId) && Objects.equals(basePrice, invTypes.basePrice) && Objects.equals(published, invTypes.published) && Objects.equals(marketGroupId, invTypes.marketGroupId) && Objects.equals(iconId, invTypes.iconId) && Objects.equals(soundId, invTypes.soundId) && Objects.equals(graphicId, invTypes.graphicId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeId, groupId, typeName, description, mass, volume, capacity, portionSize, raceId, basePrice, published, marketGroupId, iconId, soundId, graphicId);
    }
}
