package com.frcoa.frcoaapi.data.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="invCategories")
public class InvCategories {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "categoryID")
    private Integer categoryId;
    @Basic
    @Column(name = "categoryName")
    private String categoryName;
    @Basic
    @Column(name = "iconID")
    private Integer iconId;
    @Basic
    @Column(name = "published")
    private Byte published;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getIconId() {
        return iconId;
    }

    public void setIconId(Integer iconId) {
        this.iconId = iconId;
    }

    public Byte getPublished() {
        return published;
    }

    public void setPublished(Byte published) {
        this.published = published;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvCategories that = (InvCategories) o;
        return categoryId == that.categoryId && Objects.equals(categoryName, that.categoryName) && Objects.equals(iconId, that.iconId) && Objects.equals(published, that.published);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryId, categoryName, iconId, published);
    }
}
