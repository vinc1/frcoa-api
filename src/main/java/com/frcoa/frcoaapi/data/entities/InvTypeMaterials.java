package com.frcoa.frcoaapi.data.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="invTypeMaterials")
@IdClass(InvTypeMaterialsPK.class)
public class InvTypeMaterials {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "typeID")
    private Integer typeId;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "materialTypeID")
    private Integer materialTypeId;
    @Basic
    @Column(name = "quantity")
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "typeID", referencedColumnName = "typeID", insertable = false, updatable = false)
    private InvTypes type;

    @ManyToOne
    @JoinColumn(name = "materialTypeID", referencedColumnName = "typeID", insertable = false, updatable = false)
    private InvTypes material;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(Integer materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public InvTypes getMaterial() {
        return material;
    }

    public void setMaterial(InvTypes material) {
        this.material = material;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvTypeMaterials that = (InvTypeMaterials) o;
        return typeId == that.typeId && materialTypeId == that.materialTypeId && quantity == that.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeId, materialTypeId, quantity);
    }
}
