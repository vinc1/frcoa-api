package com.frcoa.frcoaapi.data.entities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class InvTypeMaterialsPK implements Serializable {
    @Column(name = "typeID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer typeId;
    @Column(name = "materialTypeID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer materialTypeId;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(Integer materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvTypeMaterialsPK that = (InvTypeMaterialsPK) o;
        return typeId == that.typeId && materialTypeId == that.materialTypeId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeId, materialTypeId);
    }
}
