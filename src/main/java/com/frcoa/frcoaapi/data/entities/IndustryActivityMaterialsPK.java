package com.frcoa.frcoaapi.data.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

public class IndustryActivityMaterialsPK implements Serializable {
    @Basic
    @Id
    @Column(name = "typeID")
    private Integer typeId;
    @Basic
    @Id
    @Column(name = "activityID")
    private Integer activityId;
    @Basic
    @Id
    @Column(name = "materialTypeID")
    private Integer materialTypeId;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public Integer getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(Integer materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndustryActivityMaterialsPK that = (IndustryActivityMaterialsPK) o;
        return Objects.equals(typeId, that.typeId) && Objects.equals(activityId, that.activityId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeId, activityId);
    }
}
