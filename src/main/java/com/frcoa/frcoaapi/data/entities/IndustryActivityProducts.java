package com.frcoa.frcoaapi.data.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "industryActivityProducts")
@IdClass(IndustryActivityProductsPK.class)
public class IndustryActivityProducts {
    @Basic
    @Id
    @Column(name = "typeID")
    private Integer typeId;
    @Basic
    @Id
    @Column(name = "activityID")
    private Integer activityId;
    @Basic
    @Column(name = "productTypeID")
    private Integer productTypeId;
    @Basic
    @Column(name = "quantity")
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "productTypeID", referencedColumnName = "typeID", insertable = false, updatable = false)
    private InvTypes productType;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "typeID", referencedColumnName = "typeId", insertable = false, updatable = false)
    private InvTypes bpType;

    @OneToMany(mappedBy = "productRef")
    private Collection<IndustryActivityMaterials> IndustrialMaterials;


    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public InvTypes getBpType() {
        return bpType;
    }

    public void setBpType(InvTypes bpType) {
        this.bpType = bpType;
    }

    public Collection<IndustryActivityMaterials> getIndustrialMaterials() {
        return IndustrialMaterials;
    }

    public void setIndustrialMaterials(Collection<IndustryActivityMaterials> industrialMaterials) {
        IndustrialMaterials = industrialMaterials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndustryActivityProducts that = (IndustryActivityProducts) o;
        return Objects.equals(typeId, that.typeId) && Objects.equals(activityId, that.activityId) && Objects.equals(productTypeId, that.productTypeId) && Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeId, activityId, productTypeId, quantity);
    }
}
