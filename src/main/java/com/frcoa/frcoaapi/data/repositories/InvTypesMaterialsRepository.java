package com.frcoa.frcoaapi.data.repositories;

import com.frcoa.frcoaapi.data.entities.InvTypeMaterials;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvTypesMaterialsRepository extends JpaRepository<InvTypeMaterials,Integer> {
}
