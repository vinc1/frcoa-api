package com.frcoa.frcoaapi.data.repositories.selector;

import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.data.repositories.InvTypesRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

@Primary
public interface SelectorInvTypesRepository extends InvTypesRepository {

    @Query("SELECT DISTINCT t FROM invTypes t " +
            "JOIN t.group grp " +
            "JOIN grp.category cat " +
            "WHERE cat.categoryId = :IdCategory ")
    public List<InvTypes> getByCategoryId(Integer IdCategory);

    @Query("SELECT DISTINCT t FROM invTypes t "+
            "WHERE t.typeName LIKE %:search% ")
    public List<InvTypes> getBySearch(String search);
}
