package com.frcoa.frcoaapi.data.repositories.selector;

import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.data.repositories.InvTypesRepository;
import com.frcoa.frcoaapi.data.repositories.ItemPricesRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

@Primary
public interface SelectorItemPrices extends ItemPricesRepository {

}
