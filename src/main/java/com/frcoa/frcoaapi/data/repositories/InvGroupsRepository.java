package com.frcoa.frcoaapi.data.repositories;

import com.frcoa.frcoaapi.data.entities.InvGroups;
import com.frcoa.frcoaapi.data.entities.InvTypeMaterials;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvGroupsRepository extends JpaRepository<InvGroups,Integer> {
}
