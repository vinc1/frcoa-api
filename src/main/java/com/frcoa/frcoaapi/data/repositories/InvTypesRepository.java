package com.frcoa.frcoaapi.data.repositories;

import com.frcoa.frcoaapi.data.entities.InvTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface InvTypesRepository extends JpaRepository<InvTypes,Integer> {
    public InvTypes getInvTypesByTypeId(Integer typeid);
    public List<InvTypes> getInvTypesByGroupId(Integer groupId);
}
