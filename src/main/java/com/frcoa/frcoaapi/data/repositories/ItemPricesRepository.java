package com.frcoa.frcoaapi.data.repositories;

import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.data.entities.ItemPrices;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ItemPricesRepository extends JpaRepository<ItemPrices,Integer> {
    Optional<ItemPrices> findItemPricesByTypeId(Integer typeId);
}
