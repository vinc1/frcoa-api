package com.frcoa.frcoaapi.data.repositories;

import com.frcoa.frcoaapi.data.entities.InvCategories;
import com.frcoa.frcoaapi.data.entities.InvGroups;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvCategoriesRepository extends JpaRepository<InvCategories,Integer> {
}
