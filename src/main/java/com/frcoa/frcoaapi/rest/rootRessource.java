package com.frcoa.frcoaapi.rest;

import com.frcoa.frcoaapi.data.entities.InvTypeMaterials;
import com.frcoa.frcoaapi.data.entities.InvTypeMaterialsPK;
import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.data.entities.ItemPrices;
import com.frcoa.frcoaapi.data.repositories.InvCategoriesRepository;
import com.frcoa.frcoaapi.data.repositories.InvGroupsRepository;
import com.frcoa.frcoaapi.data.repositories.InvTypesMaterialsRepository;
import com.frcoa.frcoaapi.data.repositories.InvTypesRepository;
import com.frcoa.frcoaapi.data.repositories.selector.SelectorInvTypesRepository;
import com.frcoa.frcoaapi.data.repositories.selector.SelectorItemPrices;
import com.frcoa.frcoaapi.dto.*;
import com.frcoa.frcoaapi.dto.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Array;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping(value="/")
public class rootRessource {
    @Autowired
    private InvTypesRepository invTypesRepository;
    @Autowired
    private InvTypesDtoMapper invTypesDtoMapper;
    @Autowired
    private InvTypesSimpleDtoMapper invTypesSimpleDtoMapper;
    @Autowired
    private InvTypesMaterialsRepository invTypesMaterialsRepository;
    @Autowired
    private InvTypesProdDtoMapper invTypesProdDtoMapper;
    @Autowired
    private InvGroupsRepository invGroupsRepository;
    @Autowired
    private InvGroupsDtoMapper invGroupsDtoMapper;
    @Autowired
    private InvCategoriesRepository invCategoriesRepository;
    @Autowired
    private InvCategoriesDtoMapper invCategoriesDtoMapper;
    @Autowired
    private SelectorInvTypesRepository selectorInvTypesRepository;
    @RequestMapping(method = RequestMethod.GET)
    public List<InvTypesSimpleDto> getTypesSimple()
    {
        return invTypesSimpleDtoMapper.allToDtos(invTypesRepository.findAll());
    }

    @RequestMapping(value = "/raw",method = RequestMethod.GET)
    public List<InvTypes> getTypesRaw()
    {
        return invTypesRepository.findAll();
    }

    @RequestMapping(value = "/groups",method = RequestMethod.GET)
    public List<InvGroupsDto> getGroups()
    {
        return invGroupsDtoMapper.allToDtos(invGroupsRepository.findAll());
    }
    @RequestMapping(value = "/categories",method = RequestMethod.GET)
    public List<InvCategoriesDto> getCategories()
    {
        return invCategoriesDtoMapper.allToDtos(invCategoriesRepository.findAll());
    }
    @RequestMapping(params = {"groupid"}, method = RequestMethod.GET)
    public List<InvTypesSimpleDto> getTypesSimpleByGroupId(@RequestParam(required = true) Integer groupid)
    {
        return  invTypesSimpleDtoMapper.allToDtos(invTypesRepository.getInvTypesByGroupId(groupid));
    }
    @RequestMapping(params = {"categoryid"}, method = RequestMethod.GET)
    public List<InvTypesSimpleDto> getTypesSimpleByCategoriyId(@RequestParam(required = true) Integer categoryid)
    {
        return  invTypesSimpleDtoMapper.allToDtos(selectorInvTypesRepository.getByCategoryId(categoryid));
    }
    @RequestMapping(params = {"search"}, method = RequestMethod.GET)
    public List<InvTypesSimpleDto> getTypesSimpleBySearch(@RequestParam(required = true) String search)
    {
        return  invTypesSimpleDtoMapper.allToDtos(selectorInvTypesRepository.getBySearch(search));
    }
    @RequestMapping(value="/{typeId}", method= RequestMethod.GET)
    public InvTypesDto getOne(@PathVariable Integer typeId)
    {
        return invTypesDtoMapper.toDto(invTypesRepository.getInvTypesByTypeId(typeId));
    }

    @RequestMapping(value="/{typeId}/raw", method= RequestMethod.GET)
    public InvTypes getOneRaw(@PathVariable Integer typeId)
    {
        return invTypesRepository.getInvTypesByTypeId(typeId);
    }

    @RequestMapping(value="/{typeId}/prod", method= RequestMethod.GET)
    public InvTypesProdDto getOneProd(@PathVariable Integer typeId)
    {
        return invTypesProdDtoMapper.toDto(invTypesRepository.getInvTypesByTypeId(typeId));
    }

    private Map<String, Integer> retoursDivers;
    private Map<String, Integer> retoursBp;
    private Map<String, Integer> retourMaterials;
    private Map<String, Integer> retourComodity;
    private Map<String, Integer> retourPlanetaryComodity;
    private Map<String, Integer> retourCelestial;
    Map<String, Object> retourtot;
    List<Map<String, Object>> rtn;
    List<Map<String, Object>> rtnlvl;
    @RequestMapping(value="/{typeId}/prod/total", method= RequestMethod.GET)
    public List<Map<String, Object>> getOneProdtotal(@PathVariable Integer typeId, @RequestParam(required = false, defaultValue = "1")Integer quantity, @RequestParam(required = false, defaultValue = "0")Integer me, @RequestParam(required = false)List<Integer> to_buy)
    {
        if(to_buy == null)
        {
            to_buy = new ArrayList<>() ;
        }
        InvTypesDto a_prod = invTypesDtoMapper.toDto(invTypesRepository.getInvTypesByTypeId(typeId));
        rtn = new ArrayList<>();
        rtnlvl = new ArrayList<>();
        retourtot = new LinkedHashMap<>();
        retourtot.put("Grand Total",null);
        parseProdByBP(a_prod, 0, quantity,me, to_buy);
        rtn.addAll(rtnlvl);
        rtn.add(retourtot);
        return rtn;
    }
    public void parseProdByBP(InvTypesDto a_prod, Integer lvl, Integer quantity, Integer me,List<Integer> to_buy)
    {
        try {
            rtnlvl.get(lvl);
        }catch (IndexOutOfBoundsException e) {
            rtnlvl.add(lvl, new LinkedHashMap<>());
            rtnlvl.get(lvl).put("Total Level",lvl);
        }
        Double taux_me = null;
        if(me != 0)
            taux_me = me.doubleValue() / 100;
        Map<String, Object> retour = new LinkedHashMap<>();
        rtn.add(retour);
        retour.put(a_prod.getTypeName(),"A Produire:"+quantity.toString());
        retour.put("typeId",a_prod.getTypeId());
        retour.put("level", lvl);
        retour.put("url", "https://frcoa-api.vginfoservice.fr/"+a_prod.getTypeId()+"/prod/total?quantity="+quantity+"&me="+me);
        if(!a_prod.getBlueprints().isEmpty()) {
            retour.put(a_prod.getBlueprints().get(0).getBpType().getTypeName(), "Produit(1 Run):"+a_prod.getBlueprints().get(0).getQuantity());
            int nb_runs = (int) Math.ceil(quantity.floatValue() / a_prod.getBlueprints().get(0).getQuantity().floatValue());
            retour.put("Nombre de runs", nb_runs);
            for (IndustryActivityMaterialsDto material : a_prod.getBlueprints().get(0).getIndustrialMaterials()) {
                int material_me;
                if (taux_me != null && a_prod.getBlueprints().get(0).getBpType().getGroup().getGroupId() != 1888)
                    material_me = (int) Math.ceil(material.getQuantity() - (material.getQuantity()*taux_me));
                else
                    material_me = material.getQuantity();
                String keyName = material.getMaterialProductType().getTypeName()+" - "+material.getMaterialProductType().getGroup().getGroupName();
                Integer totQuantity = material_me*nb_runs;
                Locale locale = new Locale("is", "IS");
                NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);
                formatter.setMaximumFractionDigits(2);
                formatter.setMinimumFractionDigits(2);
                retour.put(keyName, totQuantity);
                if(!material.getMaterialProductType().getBlueprints().isEmpty() && !to_buy.contains(material.getMaterialTypeId()))
                    parseProdByBP(material.getMaterialProductType(),lvl+1, material_me*nb_runs,me, to_buy);
                else
                {
                    Integer totQuantityCumul;
                    if(retourtot.containsKey(keyName))
                        totQuantityCumul = (Integer)retourtot.get(keyName)+totQuantity;
                    else
                        totQuantityCumul = totQuantity;
                    retourtot.put(keyName, totQuantityCumul);
                    retourtot.put("Prix - "+keyName,formatter.format(getPrice(material.getMaterialTypeId()).getPrice()*totQuantityCumul)+" - "+formatter.format(getPrice(material.getMaterialTypeId()).getPrice())+"/unit");
                    Integer totQuantityCumullvl;
                    if(rtnlvl.get(lvl).containsKey(keyName))
                        totQuantityCumullvl = (Integer)rtnlvl.get(lvl).get(keyName)+(totQuantity);
                    else
                        totQuantityCumullvl = totQuantity;
                    rtnlvl.get(lvl).put(keyName, totQuantityCumullvl);
                    rtnlvl.get(lvl).put("Prix - "+keyName,formatter.format(getPrice(material.getMaterialTypeId()).getPrice()*totQuantityCumullvl)+" - "+formatter.format(getPrice(material.getMaterialTypeId()).getPrice())+"/unit");
                }
            }
        }
    }

    @Autowired
    SelectorItemPrices selectorItemPrices;
    @RequestMapping(value="/{typeId}/prices", method= RequestMethod.GET)
    public ItemPrices getPrice(@PathVariable Integer typeId)
    {
        ItemPrices price;
        if(selectorItemPrices.findItemPricesByTypeId(typeId).isPresent()) {
            price = selectorItemPrices.findItemPricesByTypeId(typeId).get();
            Date now = Timestamp.from(Instant.now());
            Date cache_creation = price.getDate();
            long diffInMillies = Math.abs(now.getTime() - cache_creation.getTime());
            long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            if(diff >= 2) {
                updatePrices();
                price = selectorItemPrices.findItemPricesByTypeId(typeId).get();
            }
        }
        else
        {
            updatePrices();
            price = selectorItemPrices.findItemPricesByTypeId(typeId).get();
        }
        return price;
    }

    private void updatePrices()
    {
        selectorItemPrices.deleteAll();
        selectorItemPrices.flush();
        List<ItemPrices> itemPrices = selectorItemPrices.findAll();
        Timestamp time = Timestamp.from(Instant.now());
        String url = "https://esi.evetech.net/latest/markets/prices/?datasource=tranquility";
        RestTemplate restTemplate = new RestTemplate();
        Object[] prices = restTemplate.getForObject(url,Object[].class);
        for (Object price : prices) {
            ItemPrices prix = new ItemPrices();
            prix.setId(0);
            if(((LinkedHashMap<String, Object>)price).get("type_id") != null)
                prix.setTypeId((int)((LinkedHashMap<String, Object>)price).get("type_id"));
            if(((LinkedHashMap<String, Object>)price).get("average_price") != null)
                prix.setPrice((double)((LinkedHashMap<String, Object>)price).get("average_price"));
            prix.setDate(time);
            itemPrices.add(prix);
        }
        selectorItemPrices.saveAllAndFlush(itemPrices);
    }
}
