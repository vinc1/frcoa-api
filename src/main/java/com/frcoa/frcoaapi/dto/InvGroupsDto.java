package com.frcoa.frcoaapi.dto;

import com.frcoa.frcoaapi.data.entities.InvGroups;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

public class InvGroupsDto {
    private Integer groupId;
    private Integer categoryId;
    private String groupName;
    private InvCategoriesDto category;


    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public InvCategoriesDto getCategory() {
        return category;
    }

    public void setCategory(InvCategoriesDto category) {
        this.category = category;
    }
}
