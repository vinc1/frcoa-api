package com.frcoa.frcoaapi.dto;

import java.math.BigDecimal;
import java.util.List;

public class InvTypesSimpleDto {
    private Integer typeId;
    private String typeName;
    private Integer raceId;
    private BigDecimal basePrice;
    private InvGroupsDto group;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getRaceId() {
        return raceId;
    }

    public void setRaceId(Integer raceId) {
        this.raceId = raceId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public InvGroupsDto getGroup() {
        return group;
    }

    public void setGroup(InvGroupsDto group) {
        this.group = group;
    }
}
