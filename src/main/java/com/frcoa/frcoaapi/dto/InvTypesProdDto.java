package com.frcoa.frcoaapi.dto;

import java.math.BigDecimal;
import java.util.List;

public class InvTypesProdDto {
    private Integer typeId;
    private String typeName;
    private List<IndustryActivityProductsProdDto> blueprints;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<IndustryActivityProductsProdDto> getBlueprints() {
        return blueprints;
    }

    public void setBlueprints(List<IndustryActivityProductsProdDto> blueprints) {
        this.blueprints = blueprints;
    }
}
