package com.frcoa.frcoaapi.dto;

import java.util.Collection;

public class IndustryActivityProductsProdDto {

    private Integer typeId;

    private Integer quantity;

    private String bpTypeName;

    private Collection<IndustryActivityMaterialsProdDto> IndustrialMaterials;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getBpTypeName() {
        return bpTypeName;
    }

    public void setBpTypeName(String bpTypeName) {
        this.bpTypeName = bpTypeName;
    }

    public Collection<IndustryActivityMaterialsProdDto> getIndustrialMaterials() {
        return IndustrialMaterials;
    }

    public void setIndustrialMaterials(Collection<IndustryActivityMaterialsProdDto> industrialMaterials) {
        IndustrialMaterials = industrialMaterials;
    }
}
