package com.frcoa.frcoaapi.dto;

import com.frcoa.frcoaapi.data.entities.IndustryActivityMaterials;
import com.frcoa.frcoaapi.data.entities.InvTypes;

import javax.persistence.*;
import java.util.Collection;

public class IndustryActivityProductsDto {

    private Integer typeId;

    private Integer quantity;

    private InvTypesDto bpType;

    private Collection<IndustryActivityMaterialsDto> IndustrialMaterials;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public InvTypesDto getBpType() {
        return bpType;
    }

    public void setBpType(InvTypesDto bpType) {
        this.bpType = bpType;
    }

    public Collection<IndustryActivityMaterialsDto> getIndustrialMaterials() {
        return IndustrialMaterials;
    }

    public void setIndustrialMaterials(Collection<IndustryActivityMaterialsDto> industrialMaterials) {
        IndustrialMaterials = industrialMaterials;
    }
}
