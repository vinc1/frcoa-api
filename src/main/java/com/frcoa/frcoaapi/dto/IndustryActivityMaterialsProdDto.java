package com.frcoa.frcoaapi.dto;

public class IndustryActivityMaterialsProdDto {
    private Integer materialTypeId;
    private Integer quantity;
    private String MaterialTypeName;
    private InvTypesProdDto materialProductType;

    public Integer getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(Integer materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getMaterialTypeName() {
        return MaterialTypeName;
    }

    public void setMaterialTypeName(String materialTypeName) {
        MaterialTypeName = materialTypeName;
    }

    public InvTypesProdDto getMaterialProductType() {
        return materialProductType;
    }

    public void setMaterialProductType(InvTypesProdDto materialProductType) {
        this.materialProductType = materialProductType;
    }
}
