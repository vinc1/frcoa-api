package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.IndustryActivityProducts;
import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.dto.IndustryActivityProductsDto;
import com.frcoa.frcoaapi.dto.InvTypesDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {InvTypesDtoMapper.class})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface IndustryActivityProductsDtoMapper extends GenericMapper<IndustryActivityProductsDto, IndustryActivityProducts>{
    @Override
    @Mappings({

    })
    IndustryActivityProductsDto toDto(IndustryActivityProducts model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<IndustryActivityProductsDto> allToDtos(List<IndustryActivityProducts> models);
}
