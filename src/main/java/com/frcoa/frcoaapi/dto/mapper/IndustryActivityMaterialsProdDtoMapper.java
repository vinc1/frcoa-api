package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.IndustryActivityMaterials;
import com.frcoa.frcoaapi.dto.IndustryActivityMaterialsDto;
import com.frcoa.frcoaapi.dto.IndustryActivityMaterialsProdDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface IndustryActivityMaterialsProdDtoMapper extends GenericMapper<IndustryActivityMaterialsProdDto, IndustryActivityMaterials>{
    @Override
    @Mappings({
        @Mapping(source = "materialProductType.typeName", target = "materialTypeName")
    })
    IndustryActivityMaterialsProdDto toDto(IndustryActivityMaterials model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<IndustryActivityMaterialsProdDto> allToDtos(List<IndustryActivityMaterials> models);
}
