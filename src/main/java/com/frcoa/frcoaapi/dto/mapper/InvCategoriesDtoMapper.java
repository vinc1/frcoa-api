package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.InvCategories;
import com.frcoa.frcoaapi.data.entities.InvGroups;
import com.frcoa.frcoaapi.dto.InvCategoriesDto;
import com.frcoa.frcoaapi.dto.InvGroupsDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface InvCategoriesDtoMapper extends GenericMapper<InvCategoriesDto, InvCategories>{
    @Override
    @Mappings({

    })
    InvCategoriesDto toDto(InvCategories model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<InvCategoriesDto> allToDtos(List<InvCategories> models);
}
