package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.InvGroups;
import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.dto.InvGroupsDto;
import com.frcoa.frcoaapi.dto.InvTypesDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface InvGroupsDtoMapper extends GenericMapper<InvGroupsDto, InvGroups>{
    @Override
    @Mappings({

    })
    InvGroupsDto toDto(InvGroups model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<InvGroupsDto> allToDtos(List<InvGroups> models);
}
