package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.InvTypeMaterials;
import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.dto.InvTypeMaterialsDto;
import com.frcoa.frcoaapi.dto.InvTypesDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {InvTypesDtoMapper.class})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface InvTypeMaterialsDtoMapper extends GenericMapper<InvTypeMaterialsDto, InvTypeMaterials>{
    @Override
    @Mappings({
        @Mapping(source = "material.typeName", target = "materialName")
    })
    InvTypeMaterialsDto toDto(InvTypeMaterials model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<InvTypeMaterialsDto> allToDtos(List<InvTypeMaterials> models);
}
