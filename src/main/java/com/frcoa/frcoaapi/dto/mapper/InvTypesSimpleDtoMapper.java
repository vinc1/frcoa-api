package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.dto.InvTypesDto;
import com.frcoa.frcoaapi.dto.InvTypesSimpleDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface InvTypesSimpleDtoMapper extends GenericMapper<InvTypesSimpleDto, InvTypes>{
    @Override
    @Mappings({

    })
    InvTypesSimpleDto toDto(InvTypes model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<InvTypesSimpleDto> allToDtos(List<InvTypes> models);
}
