package com.frcoa.frcoaapi.dto.mapper;

import org.mapstruct.MappingTarget;

import java.util.List;

public interface GenericMapper<T, D> {
    T toDto(D model);
    List<T> allToDtos(List<D> models);
}
