package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.IndustryActivityMaterials;
import com.frcoa.frcoaapi.data.entities.IndustryActivityProducts;
import com.frcoa.frcoaapi.dto.IndustryActivityMaterialsDto;
import com.frcoa.frcoaapi.dto.IndustryActivityProductsDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {InvTypesDtoMapper.class})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface IndustryActivityMaterialsDtoMapper extends GenericMapper<IndustryActivityMaterialsDto, IndustryActivityMaterials>{
    @Override
    @Mappings({

    })
    IndustryActivityMaterialsDto toDto(IndustryActivityMaterials model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<IndustryActivityMaterialsDto> allToDtos(List<IndustryActivityMaterials> models);
}
