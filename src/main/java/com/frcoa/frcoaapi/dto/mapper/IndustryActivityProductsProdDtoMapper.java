package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.IndustryActivityProducts;
import com.frcoa.frcoaapi.dto.IndustryActivityProductsDto;
import com.frcoa.frcoaapi.dto.IndustryActivityProductsProdDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {IndustryActivityMaterialsProdDtoMapper.class})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface IndustryActivityProductsProdDtoMapper extends GenericMapper<IndustryActivityProductsProdDto, IndustryActivityProducts>{
    @Override
    @Mappings({
        @Mapping(source = "bpType.typeName", target = "bpTypeName")
    })
    IndustryActivityProductsProdDto toDto(IndustryActivityProducts model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<IndustryActivityProductsProdDto> allToDtos(List<IndustryActivityProducts> models);
}
