package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.dto.InvTypesDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface InvTypesDtoMapper extends GenericMapper<InvTypesDto, InvTypes>{
    @Override
    @Mappings({

    })
    InvTypesDto toDto(InvTypes model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<InvTypesDto> allToDtos(List<InvTypes> models);
}
