package com.frcoa.frcoaapi.dto.mapper;

import com.frcoa.frcoaapi.data.entities.InvTypes;
import com.frcoa.frcoaapi.dto.InvTypesDto;
import com.frcoa.frcoaapi.dto.InvTypesProdDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {IndustryActivityProductsProdDtoMapper.class})
@MapperConfig(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface InvTypesProdDtoMapper extends GenericMapper<InvTypesProdDto, InvTypes>{
    @Override
    @Mappings({
    })
    InvTypesProdDto toDto(InvTypes model);

    @Override
    @InheritConfiguration(name = "toDto")
    List<InvTypesProdDto> allToDtos(List<InvTypes> models);
}
