package com.frcoa.frcoaapi.dto;

import com.frcoa.frcoaapi.data.entities.InvGroups;
import com.frcoa.frcoaapi.data.entities.InvTypeMaterials;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

public class InvTypesDto {
    private Integer typeId;
    private String typeName;
    private Integer raceId;
    private BigDecimal basePrice;
    private InvGroupsDto group;
    //private List<InvTypeMaterialsDto> materiaux;
    private List<IndustryActivityProductsDto> blueprints;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getRaceId() {
        return raceId;
    }

    public void setRaceId(Integer raceId) {
        this.raceId = raceId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public InvGroupsDto getGroup() {
        return group;
    }

    public void setGroup(InvGroupsDto group) {
        this.group = group;
    }

    /*public List<InvTypeMaterialsDto> getMateriaux() {
        return materiaux;
    }

    public void setMateriaux(List<InvTypeMaterialsDto> materiaux) {
        this.materiaux = materiaux;
    }*/

    public List<IndustryActivityProductsDto> getBlueprints() {
        return blueprints;
    }

    public void setBlueprints(List<IndustryActivityProductsDto> blueprints) {
        this.blueprints = blueprints;
    }
}
