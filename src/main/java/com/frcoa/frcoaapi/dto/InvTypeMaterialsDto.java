package com.frcoa.frcoaapi.dto;

import com.frcoa.frcoaapi.data.entities.InvGroups;

import javax.persistence.*;
import java.math.BigDecimal;

public class InvTypeMaterialsDto {
    private Integer typeId;
    private Integer materialTypeId;
    private Integer quantity;
    private String materialName;
    private InvTypesDto material;
    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(Integer materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public InvTypesDto getMaterial() {
        return material;
    }

    public void setMaterial(InvTypesDto material) {
        this.material = material;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }
}
