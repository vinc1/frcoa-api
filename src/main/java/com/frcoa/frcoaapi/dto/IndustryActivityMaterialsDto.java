package com.frcoa.frcoaapi.dto;

import com.frcoa.frcoaapi.data.entities.IndustryActivityProducts;
import com.frcoa.frcoaapi.data.entities.InvTypes;

import javax.persistence.*;

public class IndustryActivityMaterialsDto {
    private Integer materialTypeId;
    private Integer quantity;
    private InvTypesDto materialProductType;

    public Integer getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(Integer materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public InvTypesDto getMaterialProductType() {
        return materialProductType;
    }

    public void setMaterialProductType(InvTypesDto materialProductType) {
        this.materialProductType = materialProductType;
    }
}
