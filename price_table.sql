create table sde_eve_online.item_prices
(
    id int auto_increment
        primary key,
    type_id int,
    price   double   null,
    date    datetime null
);

